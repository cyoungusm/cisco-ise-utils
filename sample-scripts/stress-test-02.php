<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use ISEUtils\Uuid;
use ISEUtils\MAC;
use ISEUtils\Helpers;
use ISEUtils\Exceptions\ProtocolException;
use ISEUtils\ERS\ConnectionParams;
use ISEUtils\ISE\Client;
use ISEUtils\ISE\EndPoint;
use ISEUtils\ISE\EndPointGroup;
use Psr\Log\NullLogger;
use Wa72\SimpleLogger\EchoLogger;

require_once 'vendor/autoload.php';

require __DIR__ . '/settings-default.php';
file_exists(__DIR__ . '/settings.php') AND include __DIR__ . '/settings.php';

$logger = new EchoLogger();

$pagelimit = 0;
//$numEndpointsToDelete = 600;
$numEndpointsToDelete = 90;
$recordlimit = $numEndpointsToDelete;

$connection = new ConnectionParams($CISCO_ISE_HOST,
                                   $CISCO_ISE_ERS_PORT,
                                   $CISCO_ISE_ERS_USER,
                                   $CISCO_ISE_ERS_PASS,
                                   $pagelimit,
                                   $recordlimit,
                                   $CISCO_ISE_ERS_PROTO,
                                   $CISCO_ISE_ERS_TXPERMIN,
                                   $CISCO_ISE_ERS_TXPERCON,
                                   $CISCO_ISE_ERS_VERIFYSSL );

$iseclient = new ISEUtils\ISE\Client(
    $connection,
    [ "logger" => $logger,
      "messagelogger" => $logger,
    ]
);

$scriptArgs = $_SERVER['argv'];
$groupName = "";
$group = NULL;

if (! Helpers::isNull($scriptArgs) && is_array($scriptArgs) 
    && isset($scriptArgs[1]) && is_string($scriptArgs[1]) && strlen($scriptArgs[1]) > 0)
{
    $groupName = $scriptArgs[1];

} else {
    echo PHP_EOL . "Usage: " . $scriptArgs[0] . " [group name] ". PHP_EOL . PHP_EOL;
    exit(1);
}

try
{
    $group = new EndPointGroup([
        "name" => $groupName,
        "systemDefined" => false
    ]);

    $logger->notice("Checking for EndPointGroup ". $group->getName());

    $res = $iseclient->getEndPointGroup('name/' . $group->getName());

    if ( Helpers::isNull($res) )
    {
        $logger->error("\$iseclient->getEndPointGroup(name/$groupName) failed.");
        exit;
    }
    else
    {
        $group = $res;
        $logger->notice("Found $groupName.");
    }

}
catch (ProtocolException $e)
{
    $logger->notice("Exception! Failed to create EndPointGroup");
    $logger->notice($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}



try
{
    $endpoints = $iseclient->getEndPointsByGroup($group);

    if (! Helpers::isNull($endpoints) )
    {
        $logger->notice("Got " . count($endpoints) . " EndPoints");

        foreach ($endpoints as $endpoint)
        {
            if ($endpoint->getPartial())
            {
                $endpointstodelete[] = $iseclient->getEndPoint($endpoint->getId());
            }
            $logger->notice("EP:" . $endpoint->getId() . " : " . $endpoint->getName() . " : " . $endpoint->getgroupId());
        }

        foreach ($endpointstodelete as $ep)
        {
            if ($iseclient->deleteEndPoint($ep))
                $logger->notice("Deleted " . $ep->getName());
            else
                $logger->error("FAILED TO DELETE " . $ep->getName());
        }

    } else {
        $logger->warning("Got NO endpoints!");
    }

}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed to clean up EndPoints");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}

try
{
    $logger->notice("Checking EndPoints again after deletion to see if any silently failed");

    $endpoints = $iseclient->getEndPointsByGroup($group);

    if (! Helpers::isNull($endpoints) )
    {
        $logger->notice("Got " . count($endpoints) . " EndPoints");

        foreach ($endpoints as $endpoint)
        {
            if ($endpoint->getPartial())
            {
                $endpointstodelete[] = $iseclient->getEndPoint($endpoint->getId());
            }
            $logger->notice("EP:" . $endpoint->getId() . " : " . $endpoint->getName() . " : " . $endpoint->getgroupId());
        }


    } else {
        $logger->notice("Got NO endpoints, just as expected.  Good job!");
    }

}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed to query EndPoints after delete job");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}
