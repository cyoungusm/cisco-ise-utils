<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use ISEUtils\Uuid;
use ISEUtils\MAC;
use ISEUtils\Helpers;
use ISEUtils\Exceptions\ProtocolException;
use ISEUtils\ERS\ConnectionParams;
use ISEUtils\ISE\Client;
use ISEUtils\ISE\EndPoint;
use ISEUtils\ISE\EndPointGroup;
use Psr\Log\NullLogger;
use Wa72\SimpleLogger\EchoLogger;

require_once 'vendor/autoload.php';

require __DIR__ . '/settings-default.php';
file_exists(__DIR__ . '/settings.php') AND include __DIR__ . '/settings.php';

$logger = new EchoLogger();

$pagelimit = 0;
$recordlimit = 100;

$connection = new ConnectionParams($CISCO_ISE_HOST,
                                   $CISCO_ISE_ERS_PORT,
                                   $CISCO_ISE_ERS_USER,
                                   $CISCO_ISE_ERS_PASS,
                                   $pagelimit,
                                   $recordlimit,
                                   $CISCO_ISE_ERS_PROTO,
                                   $CISCO_ISE_ERS_TXPERMIN,
                                   $CISCO_ISE_ERS_TXPERCON,
                                   $CISCO_ISE_ERS_VERIFYSSL );

$iseclient = new ISEUtils\ISE\Client(
    $connection,
    [ "logger" => $logger,
      "messagelogger" => $logger,
    ]
);

$newEndPointGroup = NULL;

try
{
    // Be nice and check to see if the group already exists
    $existingGroup = $iseclient->getEndPointGroup("name/Chris_Sandbox02");

    if (Helpers::isNull($existingGroup))
    {
        $newEndPointGroup = new EndPointGroup([
            "name" => "Chris_Sandbox02",
            "description" => "Sandbox 02 for Chris to test with",
            "systemDefined" => false
        ]);

        $logger->notice("Attempting to create a new EndPointGroup ". $newEndPointGroup->getName());
    

        if ($iseclient->createEndPointGroup($newEndPointGroup))
        {
            $logger->notice("Success");

            if ($newEndPointGroup->getPartial())
            {
                $newEndPointGroup = $iseclient->getEndPointGroup("name/" . $newEndPointGroup->getName());
            }

            $logger->notice("Created EndPointGroup " . $newEndPointGroup->getID());
        }
        else
        {
            $logger->error("\$iseclient->createEndPointGroup(name/Chris_Sandbox02) failed.");
            exit;
        }

    }
    else
    {
        $newEndPointGroup = $existingGroup;
    }
}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed to create EndPointGroup");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}



try
{
    for ($i = 0; $i < 3; ++$i)
    {
        $macAddr = MAC::randomMAC();

        $logger->notice("Attempting to create a new EndPoint ($macAddr, gr:" . $newEndPointGroup->getId() . ")");

        $newEndPoint = new EndPoint([
            "name" => "$macAddr",
            "mac" => "$macAddr",
            "groupId" => $newEndPointGroup->getID(),
            "staticGroupAssignment" => true,
        ]);

        if ( $iseclient->createEndPoint($newEndPoint) )
        {
            $logger->notice("Success");

            if ($newEndPoint->getPartial())
            {
                $newEndPoint = $iseclient->getEndPoint("name/" . $newEndPoint->getName());
            }

            if (! Helpers::isNull($newEndPoint))
                $logger->notice("Created EndPoint " . $newEndPoint->getID());
            else
                $logger->error("Failed attempting to create EndPoint " . $macAddr);
        }
        

    }
}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed to create EndPoints");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}
