<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use ISEUtils\Helpers;
use ISEUtils\Exceptions\ProtocolException;
use ISEUtils\ERS\ConnectionParams;
use ISEUtils\ISE\Client;
use ISEUtils\ISE\EndPoint;
use Psr\Log\NullLogger;
use Wa72\SimpleLogger\EchoLogger;

require_once 'vendor/autoload.php';

require __DIR__ . '/settings-default.php';
file_exists(__DIR__ . '/settings.php') AND include __DIR__ . '/settings.php';

$logger = new EchoLogger();

$pagelimit = 0;
$recordlimit = 100;

$connection = new ConnectionParams($CISCO_ISE_HOST,
                                   $CISCO_ISE_ERS_PORT,
                                   $CISCO_ISE_ERS_USER,
                                   $CISCO_ISE_ERS_PASS,
                                   $pagelimit,
                                   $recordlimit,
                                   $CISCO_ISE_ERS_PROTO,
                                   $CISCO_ISE_ERS_TXPERMIN,
                                   $CISCO_ISE_ERS_TXPERCON,
                                   $CISCO_ISE_ERS_VERIFYSSL );

$iseclient = new ISEUtils\ISE\Client(
    $connection,
    [ "logger" => $logger,
      "messagelogger" => $logger,
    ]
);

try
{
    $groupquery = "name/Unknown";

    $endpointgroup = $iseclient->getEndPointGroup($groupquery);

    if (! Helpers::isNull($endpointgroup))
    {
        $logger->notice("Querying group " . $endpointgroup->getId() . " : " . $endpointgroup->getName());

        $endpoints = $iseclient->getEndPointsByGroup($endpointgroup);

        if (! Helpers::isNull($endpoints) )
        {
            $logger->notice("Got " . count($endpoints) . " EndPoints");

            foreach ($endpoints as $endpoint)
            {
                if ($endpoint->getPartial())
                {
                    $endpoint = $iseclient->getEndPoint($endpoint->getId());
                }
                $logger->notice("EP:" . $endpoint->getId() . " : " . $endpoint->getName() . " : " . $endpoint->getgroupId());
            }
        }
        else
        {
            $logger->warning("No EndPoints were found on the group");
        }
    }
    else
    {
        $logger->error("\$iseclient->getEndPointGroup($groupquery) returned null");
    }
}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed query EndPoint");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }

}

