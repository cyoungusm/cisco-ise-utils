<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use ISEUtils\Uuid;
use ISEUtils\MAC;
use ISEUtils\Helpers;
use ISEUtils\Exceptions\ProtocolException;
use ISEUtils\ERS\ConnectionParams;
use ISEUtils\ISE\Client;
use ISEUtils\ISE\EndPoint;
use ISEUtils\ISE\EndPointGroup;
use Psr\Log\NullLogger;
use Wa72\SimpleLogger\EchoLogger;

require_once 'vendor/autoload.php';

require __DIR__ . '/settings-default.php';
file_exists(__DIR__ . '/settings.php') AND include __DIR__ . '/settings.php';

$logger = new EchoLogger();

$pagelimit = 0;
$recordlimit = 100;

$connection = new ConnectionParams($CISCO_ISE_HOST,
                                   $CISCO_ISE_ERS_PORT,
                                   $CISCO_ISE_ERS_USER,
                                   $CISCO_ISE_ERS_PASS,
                                   $pagelimit,
                                   $recordlimit,
                                   $CISCO_ISE_ERS_PROTO,
                                   $CISCO_ISE_ERS_TXPERMIN,
                                   $CISCO_ISE_ERS_TXPERCON,
                                   $CISCO_ISE_ERS_VERIFYSSL );

$iseclient = new ISEUtils\ISE\Client(
    $connection,
    [ "logger" => $logger,
      "messagelogger" => $logger,
    ]
);

$endpointgroup = NULL;
$newEndPoint = NULL;

try
{
    $endpointgroup = $iseclient->getEndPointGroup("name/Chris_Sandbox02");

    if (Helpers::isNull($endpointgroup))
    {
        $logger->error("Could not find Group name/Chris_Sandbox02");
        exit;
    }

}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed query EndPointGroup");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }

    exit;
}

try
{
    $macAddr = MAC::randomMAC();

    $logger->notice("Attempting to create a new EndPoint ($macAddr)");

    $newEndPoint = new EndPoint([
        "name" => "$macAddr",
        "mac" => "$macAddr",
        "groupId" => $endpointgroup->getID(),
        "profileId" => "44031480-8c00-11e6-996c-525400b48521"
    ]);

    if ( $iseclient->createEndPoint($newEndPoint) )
    {
        $logger->notice("Success");

        if ($newEndPoint->getPartial())
        {
            $newEndPoint = $iseclient->getEndPoint("name/" . $newEndPoint->getName());
        }

        $logger->notice("Created EndPoint: " . $newEndPoint->getID());
    }

}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed to create endpoint");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
    
}


try
{
    $logger->notice("Deleting EndPoint");

    if ($newEndPoint != NULL)
    {
        $deleted = $iseclient->deleteEndPoint($newEndPoint);

        if ($deleted)
            $logger->notice("Successfully deleted EndPoint");
        else
            $logger->error("Failed to delete EndPoint");
    }

}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed to delete EndPoint");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}

