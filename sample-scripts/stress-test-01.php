<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use ISEUtils\Uuid;
use ISEUtils\MAC;
use ISEUtils\Helpers;
use ISEUtils\Exceptions\ProtocolException;
use ISEUtils\ERS\ConnectionParams;
use ISEUtils\ISE\Client;
use ISEUtils\ISE\EndPoint;
use ISEUtils\ISE\EndPointGroup;
use Psr\Log\NullLogger;
use Wa72\SimpleLogger\EchoLogger;

require_once 'vendor/autoload.php';

require __DIR__ . '/settings-default.php';
file_exists(__DIR__ . '/settings.php') AND include __DIR__ . '/settings.php';

$logger = new EchoLogger();

$pagelimit = 0;
$numEndpointsToCreate = 999;
$recordlimit = $numEndpointsToCreate * 2 + 1;

$connection = new ConnectionParams($CISCO_ISE_HOST,
                                   $CISCO_ISE_ERS_PORT,
                                   $CISCO_ISE_ERS_USER,
                                   $CISCO_ISE_ERS_PASS,
                                   $pagelimit,
                                   $recordlimit,
                                   $CISCO_ISE_ERS_PROTO,
                                   $CISCO_ISE_ERS_TXPERMIN,
                                   $CISCO_ISE_ERS_TXPERCON,
                                   $CISCO_ISE_ERS_VERIFYSSL );

$iseclient = new ISEUtils\ISE\Client(
    $connection,
    [ "logger" => $logger,
      //"messagelogger" => $logger,
    ]
);

$newEndPointGroup = NULL;
$newGroupName = "Stress_Test";
$newGroupDesc = "Testing mass creation and deletion";

try
{
    $newEndPointGroup = new EndPointGroup([
        "name" => $newGroupName,
        "description" => $newGroupDesc,
        "systemDefined" => false
    ]);

    $logger->notice("Checking for existing EndPointGroup ". $newEndPointGroup->getName());

    $res = $iseclient->getEndPointGroup('name/' . $newEndPointGroup->getName());

    if ( Helpers::isNull($res) )
    {
        $logger->notice("Attempting to create a new EndPointGroup ". $newEndPointGroup->getName());

        if ($iseclient->createEndPointGroup($newEndPointGroup))
        {
            $logger->notice("Success");

            if ($newEndPointGroup->getPartial())
            {
                $logger->notice("Pulling full group info");
                $newEndPointGroup = $iseclient->getEndPointGroup("name/" . $newEndPointGroup->getName());
            }

            $logger->notice("Created EndPointGroup.");
        }
        else
        {
            $logger->error("\$iseclient->createEndPointGroup(name/$newGroupName) failed.");
            exit;
        }

    }
    else
    {
        // It already existed.  Use it.
        $newEndPointGroup = $res;

        $logger->notice("Re-using existing group.");
    }

}
catch (ProtocolException $e)
{
    $logger->notice("Exception! Failed to create EndPointGroup");
    $logger->notice($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}


try
{
    for ($i = 0; $i < $numEndpointsToCreate; ++$i)
    {
        $macAddr = MAC::randomMAC();

        $logger->notice("Attempting to create a new EndPoint ($macAddr, gr:" . $newEndPointGroup->getId() . ")");

        $newEndPoint = new EndPoint([
            "name" => "$macAddr",
            "mac" => "$macAddr",
            "groupId" => $newEndPointGroup->getID(),
            "staticGroupAssignment" => true,
        ]);

        if ( $iseclient->createEndPoint($newEndPoint) )
        {
            $logger->notice("Success");

            $logger->notice("Created: EndPoint($macAddr, ". $newEndPointGroup->getId() . ")");
        }
    }
}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed to create EndPoints");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}

try
{
    $endpoints = $iseclient->getEndPointsByGroup($newEndPointGroup);

    if (! Helpers::isNull($endpoints) )
    {
        $logger->notice("Got " . count($endpoints) . " EndPoints");

        foreach ($endpoints as $endpoint)
        {
            if ($endpoint->getPartial())
            {
                $endpointstodelete[] = $iseclient->getEndPoint($endpoint->getId());
            }
            $logger->notice("EP:" . $endpoint->getId() . " : " . $endpoint->getName() . " : " . $endpoint->getgroupId());
        }
    }
}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed to query for EndPoints post creation");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}

