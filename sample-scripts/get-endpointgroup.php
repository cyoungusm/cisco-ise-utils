<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use ISEUtils\Helpers;
use ISEUtils\Exceptions\ProtocolException;
use ISEUtils\ERS\ConnectionParams;
use ISEUtils\ISE\Client;
use ISEUtils\ISE\EndPointGroup;
use Psr\Log\NullLogger;
use Wa72\SimpleLogger\EchoLogger;

require_once 'vendor/autoload.php';

require __DIR__ . '/settings-default.php';
file_exists(__DIR__ . '/settings.php') AND include __DIR__ . '/settings.php';

$logger = new EchoLogger();

$pagelimit = 0;
$recordlimit = 100;

$connection = new ConnectionParams($CISCO_ISE_HOST,
                                   $CISCO_ISE_ERS_PORT,
                                   $CISCO_ISE_ERS_USER,
                                   $CISCO_ISE_ERS_PASS,
                                   $pagelimit,
                                   $recordlimit,
                                   $CISCO_ISE_ERS_PROTO,
                                   $CISCO_ISE_ERS_TXPERMIN,
                                   $CISCO_ISE_ERS_TXPERCON,
                                   $CISCO_ISE_ERS_VERIFYSSL );

$iseclient = new ISEUtils\ISE\Client(
    $connection,
    [ "logger" => $logger,
      //"messagelogger" => $logger,
    ]
);

try
{
    $endpointgroup = $iseclient->getEndPointGroup("name/Student_Devices");

    $logger->notice("EPG:" . $endpointgroup->getId() . " : " . $endpointgroup->getName());

    $endpointgroup = NULL;
    $endpointgroup = $iseclient->getEndPointGroup("ca3d92d0-ff45-11e9-bb8b-6a087bf18de7");

    $logger->notice("EPG:" . $endpointgroup->getId() . " : " . $endpointgroup->getName());

}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed query EndPoint");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }

}

