<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use ISEUtils\Helpers;
use ISEUtils\Exceptions\ProtocolException;
use ISEUtils\ERS\ConnectionParams;
use ISEUtils\ISE\Client;
use ISEUtils\ISE\EndPoint;
use Psr\Log\NullLogger;
use Wa72\SimpleLogger\EchoLogger;

require_once 'vendor/autoload.php';

require __DIR__ . '/settings-default.php';
file_exists(__DIR__ . '/settings.php') AND include __DIR__ . '/settings.php';

$logger = new EchoLogger();

$pagelimit = 0;
$recordlimit = 100;

$connection = new ConnectionParams($CISCO_ISE_HOST,
                                   $CISCO_ISE_ERS_PORT,
                                   $CISCO_ISE_ERS_USER,
                                   $CISCO_ISE_ERS_PASS,
                                   $pagelimit,
                                   $recordlimit,
                                   $CISCO_ISE_ERS_PROTO,
                                   $CISCO_ISE_ERS_TXPERMIN,
                                   $CISCO_ISE_ERS_TXPERCON,
                                   $CISCO_ISE_ERS_VERIFYSSL );

$iseclient = new ISEUtils\ISE\Client(
    $connection,
    [ "logger" => $logger,
      "messagelogger" => $logger,
    ]
);

$endPointName = "name/00:00:48:03:0C:E3";
$endPointID = "8499ba32-aa7a-11e9-8724-623ffb053f44";

try
{
    $endpoint = $iseclient->getEndPoint($endPointName);

    if (! Helpers::isNull($endpoint))
    {
        $logger->notice("EP:" . $endpoint->getId() . " : " . $endpoint->getName());

    } else {
        $logger->error("Failed to find EndPoint $endPointName");

    }

    $endpoint = NULL;

    $endpoint = $iseclient->getEndPoint($endPointID);

    if (! Helpers::isNull($endpoint))
    {
        $logger->notice("EP:" . $endpoint->getId() . " : " . $endpoint->getName());

    } else {
        $logger->error("Failed to find EndPoint $endPointID");
    }

}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed query EndPoint");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}

