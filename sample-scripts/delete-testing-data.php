<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use ISEUtils\Uuid;
use ISEUtils\MAC;
use ISEUtils\Helpers;
use ISEUtils\Exceptions\ProtocolException;
use ISEUtils\ERS\ConnectionParams;
use ISEUtils\ISE\Client;
use ISEUtils\ISE\EndPoint;
use ISEUtils\ISE\EndPointGroup;
use Psr\Log\NullLogger;
use Wa72\SimpleLogger\EchoLogger;

require_once 'vendor/autoload.php';

require __DIR__ . '/settings-default.php';
file_exists(__DIR__ . '/settings.php') AND include __DIR__ . '/settings.php';

$logger = new EchoLogger();

$pagelimit = 0;
$recordlimit = 100;

$connection = new ConnectionParams($CISCO_ISE_HOST,
                                   $CISCO_ISE_ERS_PORT,
                                   $CISCO_ISE_ERS_USER,
                                   $CISCO_ISE_ERS_PASS,
                                   $pagelimit,
                                   $recordlimit,
                                   $CISCO_ISE_ERS_PROTO,
                                   $CISCO_ISE_ERS_TXPERMIN,
                                   $CISCO_ISE_ERS_TXPERCON,
                                   $CISCO_ISE_ERS_VERIFYSSL ); 

$iseclient = new ISEUtils\ISE\Client(
    $connection,
    [ "logger" => $logger,
      //"messagelogger" => $logger,
    ]
);

$grouptodelete = NULL;
$endpointstodelete = [];

try
{
    $logger->notice("Querying EndPointGroup Chris_Sandbox02");

    $grouptodelete = $iseclient->getEndPointGroup("name/Chris_Sandbox02");

    if (! Helpers::isNull($grouptodelete))
    {
        $logger->notice("EPG:" . $grouptodelete->getId() . " : " . $grouptodelete->getName());

        $endpoints = $iseclient->getEndPointsByGroup($grouptodelete);

        if (! Helpers::isNull($endpoints) )
        {
            $logger->notice("Got " . count($endpoints) . " EndPoints");

            foreach ($endpoints as $endpoint)
            {
                if ($endpoint->getPartial())
                {
                    $endpointstodelete[] = $iseclient->getEndPoint($endpoint->getId());
                }
                $logger->notice("EP:" . $endpoint->getId() . " : " . $endpoint->getName() . " : " . $endpoint->getgroupId());
            }
        } else {
            $logger->alert("Got NO endpoints!");
        }
    }   
    else
    {
        $logger->error("\$iseclient->getEndPointGroup(name/Chris_Sandbox02) failed.");
        exit;
    }

}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed to query testing data!");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}


try
{
    $logger->notice("Deleting EndPoints...");

    foreach ($endpointstodelete as $endpoint)
    {
        $results = $iseclient->deleteEndPoint($endpoint);

        if ($results)
        {
            $logger->notice("Deleted " . $endpoint->getID());;
        } else {
            $logger->warning("Failed to delete EndPoint ". $endpoint->getId());
        }

    }
}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed to delete endpoints.");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}

try
{
    $logger->notice("Deleting EndPointGroup...");

    $deleted = $iseclient->deleteEndPointGroup($grouptodelete);

    if ($deleted)
        $logger->notice("Successfully deleted EndPointGroup");
    else
        $logger->error("Failed to delete EndPointGroup");

}
catch (ProtocolException $e)
{
    $logger->error("Exception! Failed to endpointgroup!");
    $logger->error($e->getMessage());

    $inner = $e->getPrevious();
    if (! Helpers::isNull($inner))
    {
        $response = $inner->getResponse();
        if (! Helpers::isNull($response))
            $logger->error($response->getBody()->getContents());
        else
            $logger->error("Got no response");
    }
}
