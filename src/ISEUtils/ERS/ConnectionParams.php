<?php
namespace ISEUtils\ERS;

use ISEUtils\Helpers;
use ISEUtils\Exceptions\ProtocolException;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * ISEUtils\ConnectionParams class
 *
 * Class representing the connection parameters for the Cisco REST server.
 *
 */
class ConnectionParams
{
    private $_protocol = "https";
    private $_host = "";
    private $_port = "";
    private $_uname = "";
    private $_upass = "";
    private $_pageLimit = 0;
    private $_recordLimit = 0;
    private $_txPerMin = 0;
    private $_txPerCon = 0;    
    private $_verifySSL = true;

    public function getProtocol()         { return $this->_protocol; }
    public function getHost()             { return $this->_host; }
    public function getPort()             { return $this->_port; }
    public function getUName()            { return $this->_uname; }
    public function getUPass()            { return $this->_upass; }
    public function getPageLimit()        { return $this->_pageLimit; }
    public function getRecordLimit()      { return $this->_recordLimit; }
    public function getTxPerMin()         { return $this->_txPerMin; }
    public function getTxPerCon()         { return $this->_txPerCon; }    
    public function getVerifySSL()        { return $this->_verifySSL; }

    public function setProtocol($val)     { $this->_protocol = $val; }
    public function setHost($val)         { $this->_host = $val; }
    public function setPort($val)         { $this->_port = $val; }
    public function setUName($val)        { $this->_uname = $val; }
    public function setUPass($val)        { $this->_upass = $val; }
    public function setPageLimit($val)    { $this->_pageLimit = $val; }
    public function setRecordLimit($val)  { $this->_recordLimit = $val; }
    public function setTxPerMin($val)     { $this->_txPerMin = $val; }
    public function setTxPerCon($val)     { $this->_txPerCon = $val; }    
    public function setVerifySSL($val)    { $this->_verifySSL = $val; }

    /**
     * getTxDelay
     *
     * If the transactions per minute limit is set, calculate the suggested delay to wait before
     * each transaction.
     *
     * @return int Number of milliseconds to delay before each transaction
     */
    public function getTxDelay()
    {
        $iDelay = 0;

        if ($this->getTxPerMin() > 0)
        {
            $fudge = 0.99; // idealy this would be calculated based on average transaction time

            $fDelay = ( 60000.00 / ( $this->getTxPerMin() + 1 ) ) * $fudge;

            // Minimum wait is one millisecond
            if ($fDelay < 1.0)
                $iDelay = 0;
            else
                $iDelay = (int) $fDelay;

        }

        return $iDelay;
    }

    /**
     * __construct
     *
     * @param string $host The hostname or IP address of the Cisco REST server.
     * @param string $port The TCP port of the Cisco REST server.
     * @param string $uname The user name to use when connecting to the server.
     * @param string $upass The password to use when connecting to the server.
     * @param int $pagelimit Optional - The maximum number of pages to accept from the server in a single query.
     * @param int $recordlimit Optional - The maximum number of records to accept from the server in a single query.
     * @param string $protocol Optional - The protocol to use, either https (default) or http
     * @param int $txPerMin Optional - The maximum number of transactions per minute (0 indicates unlimited)
     * @param int $txPerCon Optional - The maximum number of transactions per connection (0 indicates unlimited)
     * @param bool $verifySSL Optional - Set to false to skip SSL certificate verification
     *
     * TODO: Add better type checking and throw exceptions as needed
     */
    public function __construct($host, $port, $uname, $upass, $pagelimit = 0, $recordlimit = 0, $protocol = "https", $txPerMin = 0, $txPerCon = 0, $verifySSL = true)
    {
        if (! Helpers::isnull($host))
            $this->setHost($host);
        else
            throw new \InvalidArgumentException(__METHOD__ . " Expected param \$host");

        if (! Helpers::isnull($port))
            $this->setPort($port);
        else
            throw new \InvalidArgumentException(__METHOD__ . " Expected param \$port");

        if (! Helpers::isnull($uname))
            $this->setUName($uname);
        else
            throw new \InvalidArgumentException(__METHOD__ . " Expected param \$uname");

        if (! Helpers::isnull($upass))
            $this->setUPass($upass);
        else
            throw new \InvalidArgumentException(__METHOD__ . " Expected param \$upass");

        if (! Helpers::isnull($pagelimit))
            $this->setPageLimit($pagelimit);

        if (! Helpers::isnull($recordlimit))
            $this->setRecordLimit($recordlimit);

        if (! Helpers::isnull($protocol))
            $this->setProtocol($protocol);
        
        if (! Helpers::isNull($txPerMin))
            $this->setTxPerMin($txPerMin);

        if (! Helpers::isNull($txPerCon))
            $this->setTxPerCon($txPerCon);

        if (! Helpers::isNull($verifySSL))
            $this->setVerifySSL($verifySSL);

    }

}

?>
