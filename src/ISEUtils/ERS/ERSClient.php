<?php
namespace ISEUtils\ERS;

use ISEUtils\Helpers;
use ISEUtils\ERS\SearchResults;
use ISEUtils\ERS\ConnectionParams;
use ISEUtils\Exceptions\ProtocolException;
use ISEUtils\Encodings\JSON;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareTrait;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Spatie\GuzzleRateLimiterMiddleware\RateLimiterMiddleware;

/**
 * ISEUtils\ERS\ERSClient class
 *
 * Provides a basic client for Cisco External RESTful Services API
 *
 */
class ERSClient
{
    /**
     * This class is PSR-3 "Logger Aware"
     */
    use LoggerAwareTrait;

    private const _std_req_headers = [
        'Accept' => 'application/json',
        'Content-type' => 'application/json'
    ];

	private const _std_paging_params = [
	   'size' => '20',
	   'page' => '1'
	];

    private $_coninfo = NULL;
    private $_httpclient = NULL; // our Guzzle client handle
    private $_msgLogger = NULL;
    private $_msgLogFmt = "{method} {uri} HTTP/{version} - {code} {res_header_Content-Length} ({error})";

    private $_txCount = 0; // The number of transactions so far on the current Guzzle client handle

    private function getClient()     { return $this->_httpclient; }
    private function setClient($val) { $this->_httpclient = $val; }

    private function getConParams()     { return $this->_coninfo; }
    private function setConParams($val) { $this->_coninfo = $val; }

    private function getStdHeaders()       { return ERSClient::_std_req_headers; }
    private function getStdPagingParams()  { return ERSClient::_std_paging_params; }

    private function getMsgLogger()     { return $this->_msgLogger; }
    private function setMsgLogger($val) { $this->_msgLogger = $val; }

    private function getMsgLogFmt()     { return $this->_msgLogFmt; }
    private function setMsgLogFmt($val) { $this->_msgLogFmt = $val; }

    private function getTxCount()   { return $this->_txCount; }
    private function resetTxCount() { $this->_txCount = 0; }
    private function incTxCount()   { $this->_txCount++; }

    /**
     * doConstruct
     *
     * @param ConnectionParams $coninfo A ConnectionParams object with connection details
     * @param array $params An array with the following optional elements:
     * <ul>
     *  <li>\Psr\Log\LoggerInterface logger - A PSR-3 compliant logger object for logging library level events</li>
     *  <li>\Psr\Log\LoggerInterface messagelogger - A PSR-3 compliant logger object for logging underlying communications (Guzzle)</li>
     *  <li>string msglogfmt - Format to use for message logging</li>
     * </ul>
     *
     */
    private function doConstruct($coninfo, $params)
    {
        if (Helpers::isnull($coninfo) || ! ($coninfo instanceof ConnectionParams) )
            throw new \InvalidArgumentException(__METHOD__ . " Expected param $coninfo type ConnectionParams");
        $this->resetTxCount();

        $this->setConParams($coninfo);

        $stack = HandlerStack::create();

        if (! Helpers::isnull($params) && is_array($params) )
        {
            if (array_key_exists('logger', $params))
            {
                if (! Helpers::isnull($params['logger']) && $params['logger'] instanceof LoggerInterface)
                    $this->setLogger($params['logger']);
            }

            if (array_key_exists('msglogfmt', $params))
            {
                if (! Helpers::isnull($params['msglogfmt']))
                    $this->setMsgLogFmt($params['msglogfmt']);
            }

            if (array_key_exists('messagelogger', $params))
            {
                if (! Helpers::isnull($params['messagelogger']) && $params['messagelogger'] instanceof LoggerInterface)
                {
                    $this->setMsgLogger($params['messagelogger']);

                    $stack->push(
                        Middleware::log(
                            $this->getMsgLogger(),
                            new MessageFormatter($this->getMsgLogFmt())
                        ));
                }
            }
        }

        $base_uri = $this->getConParams()->getProtocol() . "://" . $this->getConParams()->getHost() . ":" . $this->getConParams()->getPort() . "/";

        if (! Helpers::isnull($this->getConParams()->getTxPerMin()))
            $stack->push(RateLimiterMiddleware::perMinute($this->getConParams()->getTxPerMin()));

        // Create our Guzzle client.
        $this->setClient(new \GuzzleHttp\Client([
            'base_uri' => $base_uri,
            'handler' => $stack,
            'verify' => $this->getConParams()->getVerifySSL(),
        ]));

        if (! Helpers::isnull($this->logger))
            $this->logger->debug(__METHOD__ . " Guzzle initialized. TxPerMin/TxDelay " . $this->getConParams()->getTxPerMin() . "/" . $this->getConParams()->getTxDelay() . ". VerifySSL is " . ($this->getConParams()->getVerifySSL() ? "on" : "off") . "."  );

    }

    /**
     * __construct
     *
     * @param ConnectionParams $coninfo A ConnectionParams object with connection details
     * @param array $params An array with the following optional elements:
     * <ul>
     *  <li>\Psr\Log\LoggerInterface logger - A PSR-3 compliant logger object for logging library level events</li>
     *  <li>\Psr\Log\LoggerInterface messagelogger - A PSR-3 compliant logger object for logging underlying communications (Guzzle)</li>
     *  <li>string msglogfmt - Format to use for message logging</li>
     * </ul>
     *
     */
    public function __construct($coninfo, $params)
    {
        $this->doConstruct($coninfo, $params);
    }


    /**
     * hitTxLimit
     *
     * Checks to see if the max number of transactions per connection has been reached
     *
     * @return array An array of arrays returned by the search
     */
    private function hitTxLimit()
    {
        // Do we have a configured tx limit?
        if ($this->getConParams()->getTxPerCon() > 0)
        {
            // Have we passed it?
            if ($this->getTxCount() >= $this->getConParams()->getTxPerCon())
                return true;
        }

        return false;
    }

    /**
     * incAndCheckTxCount
     *
     * Increments the transaction counter and checks to see if the limit has been hit. If so, it
     * tears down the client and builds it back again.
     *
     */
    private function incAndCheckTxCount()
    {
        $this->incTxCount();

        if ($this->hitTxLimit())
        {
            if (! Helpers::isnull($this->logger))
                $this->logger->notice(__METHOD__ . " Transaction limit hit.");

            $this->doDestruct();
            $this->doConstruct($this->getConParams(), [ "logger" => $this->logger, "messagelogger" => $this->getMsgLogger() ]);
        }
    }

    /**
     * getResources
     *
     * Get a set of resources.
     *
     * @param string $resource_type Type of resource to get, lower case
     * @param string $filter Optional search filter like "groupId.EQ.0f5e5ca0-fffa-11e9-bb8b-6a087bf18de7"
     * @return array An array of arrays returned by the search
     */
    public function getResources($resource_type, $filter = NULL)
    {
        if (Helpers::isnull($resource_type))
            throw new \InvalidArgumentException(__METHOD__ . " Expected parameter resource_type");

        $search_results = new SearchResults(["collection_type" => "resources",
                                             "pagelimit" => $this->getConParams()->getPageLimit(),
                                             "recordlimit" => $this->getConParams()->getRecordLimit(),
        ]);

        $auth = [$this->getConParams()->getUName(), $this->getConParams()->getUPass()];
        
        try
        {
            $queryParams = $this->getStdPagingParams();
            if (! Helpers::isnull($filter))
            {
                // array_unshift($queryParams, $filter);
                $queryParams["filter"] = $filter;
            }
            
            // print_r($queryParams);
            // echo PHP_EOL;

            $this->incAndCheckTxCount();

            $response = $this->getClient()->get("/ers/config/$resource_type/", [
                'headers' => $this->getStdHeaders(),
                'query' => $queryParams,
                'auth' => $auth,
                'delay' => $this->getConParams()->getTxDelay(),
            ]);

            $code = $response->getStatusCode();

            if (! preg_match('/^20[0-9]$/', $code))
                throw new ProtocolException(__METHOD__ . " HTTP call to ERS failed, code $code", $code);

            $body = (string) $response->getBody();

            $search_results->pushResults($body);

            while ($search_results->getHasMoreResults())
            {
                
                if ($search_results->hitPageLimit($search_results) || $search_results->hitRecordLimit($search_results))
                    break;

                $response = NULL;
                $body = "";

                $response = $this->_httpclient->get($search_results->getNextPageHref(),  [
                    'headers' => $this->getStdHeaders(),
                    'auth' => $auth
                ]);

                $code = $response->getStatusCode();

                if (! preg_match('/^20[0-9]$/', $code))
                    throw new ProtocolException(__METHOD__ . " HTTP subsequent call to ERS failed, code $code", $code);

                $body = (string) $response->getBody();

                $search_results->pushResults($body);
            }

            return $search_results->getResults();

        }
        catch (\GuzzleHttp\Exception\RequestException $e)
        {
            $response = $e->getResponse();
            $code = 400;  // Default to a generic client side error code

            if (! Helpers::isnull($response))
            {
                $code = $response->getStatusCode();
            }

            if ($code == 404)
            {
                // Be nice and don't throw an exception when a resource isn't found.

                if (! Helpers::isnull($this->logger))
                    $this->logger->notice(__METHOD__ . " 404 Resource Not Found");

                return NULL;
            }
            else
            {
                if (! Helpers::isnull($this->logger))
                    $this->logger->error(__METHOD__ . " Request failed with code $code");

                throw new ProtocolException(__METHOD__ . " Guzzle RequestException: " . $e->getMessage(), $code, $e);
            }
        }

    }


    /**
     * getResource
     *
     * Get a single resource.
     *
     * @param string $resource_type Type of resource to get, lower case
     * @param string $query Query parameter
     *
     * This can be just an ID like '8499ba32-aa7a-11e2-8725-623ffb053f44' or a search
     * by name like 'name/00:00:48:03:0C:3F', etc.
     *
     */
    public function getResource($resource_type, $query)
    {
        $auth = [$this->getConParams()->getUName(), $this->getConParams()->getUPass()];

        if (Helpers::isnull($resource_type) )
            throw new \InvalidArgumentException(__METHOD__ . " Expected parameter resource_type");

        if (Helpers::isnull($query))
            throw new \InvalidArgumentException(__METHOD__ . " Expected parameter query");

        try
        {
            $this->incAndCheckTxCount();

            $response = $this->getClient()->get("/ers/config/$resource_type/$query", [
                'headers' => $this->getStdHeaders(),
                'auth' => $auth,
                'delay' => $this->getConParams()->getTxDelay(),
            ]);

            $code = $response->getStatusCode();

            if (! preg_match('/^20[0-9]$/', $code))
                throw new ProtocolException(__METHOD__ . " HTTP call to ERS failed, code $code", $code);

            $body = (string) $response->getBody();
            $resource_params = JSON::decode($body);

            return $resource_params;
        }
        catch (\GuzzleHttp\Exception\RequestException $e)
        {
            $response = $e->getResponse();
            $code = 400;  // Default to a generic client side error code

            if (! Helpers::isnull($response))
            {
                $code = $response->getStatusCode();
            }

            if ($code == 404)
            {
                // Be nice and don't throw an exception when a resource isn't found.

                if (! Helpers::isnull($this->logger))
                    $this->logger->notice(__METHOD__ . " 404 Resource Not Found");

                return NULL;
            }
            else
            {
                if (! Helpers::isnull($this->logger))
                    $this->logger->error(__METHOD__ . " Request failed with code $code");

                throw new ProtocolException(__METHOD__ . " Guzzle RequestException: " . $e->getMessage(), $code, $e);
            }
        }
        catch (\InvalidArgumentException $e)
        {
            if (! Helpers::isnull($this->logger))
                $this->logger->error(__METHOD__ . " internal code error: " . $e->getMessage());

            return NULL;
        }
    }

    /**
     * postNewResource
     *
     * Create a single, new resource.
     *
     * @param ResourceBase $obj Resource to create.  Must derive from ResourceBase.
     */
    public function postNewResource($resource_type, $params)
    {
        if (Helpers::isnull($resource_type))
            throw new \InvalidArgumentException(__METHOD__ . " Expected resource type specifier \$resource_type");
            
        if (Helpers::isnull($params))
            throw new \InvalidArgumentException(__METHOD__ . " Expected resource parameters \$params");


        $auth = [$this->getConParams()->getUName(), $this->getConParams()->getUPass()];

        $headers = $this->getStdHeaders();

        try
        {
            $this->incAndCheckTxCount();

            $response = $this->getClient()->post("/ers/config/$resource_type/", [
                'headers' => $headers,
                'auth' => $auth,
                'body' => JSON::encode($params),
                'delay' => $this->getConParams()->getTxDelay(),
            ]);

            $body = (string) $response->getBody();

            $code = $response->getStatusCode();

            if (! preg_match('/^20[0-9]$/', $code))
                throw new ProtocolException(__METHOD__ . " HTTP call to ERS failed, code $code", $code);

            return true;
        }
        catch (\GuzzleHttp\Exception\RequestException $e)
        {
            $response = $e->getResponse();
            $code = 400;  // Default to a generic client side error code

            if (! Helpers::isnull($response))
            {
                $code = $response->getStatusCode();
            }

            if ($code == 404)
            {
                // Be nice and don't throw an exception when a resource isn't found.

                if (! Helpers::isnull($this->logger))
                    $this->logger->notice(__METHOD__ . " 404 Resource Not Found");

                return NULL;
            }
            else
            {
                if (! Helpers::isnull($this->logger))
                    $this->logger->error(__METHOD__ . " Request failed with code $code");

                throw new ProtocolException(__METHOD__ . " Guzzle RequestException: " . $e->getMessage(), $code, $e);
            }

        }
        catch (\InvalidArgumentException $e)
        {
            if (! Helpers::isnull($this->logger))
                $this->logger->error(__METHOD__ . " internal code error: " . $e->getMessage());

            return false;
        }

        return false;
    }


    /**
     * deleteResource
     *
     * Deletes a single resource.
     *
     * @param string $resource_type Resource type to delete.
     * @param array $params Array representation of the resource to delete.
     *
     * @return bool true if resource was deleted, false otherwise
     */
    public function deleteResource($resource_type, $query)
    {

        if (Helpers::isnull($resource_type))
            throw new \InvalidArgumentException(__METHOD__ . " Expected resource type specifier \$resource_type");
            
        if (Helpers::isnull($query))
            throw new \InvalidArgumentException(__METHOD__ . " Expected resource specifier \$query");

        $resourceSpecifier = $query;

        $auth = [$this->getConParams()->getUName(), $this->getConParams()->getUPass()];

        $headers = $this->getStdHeaders();

        try
        {
            $this->incAndCheckTxCount();

            $response = $this->getClient()->delete("/ers/config/$resource_type/$resourceSpecifier", [
                'headers' => $headers,
                'auth' => $auth,
                'delay' => $this->getConParams()->getTxDelay(),
            ]);

            $body = (string) $response->getBody();

            // print_r($body);

            $code = $response->getStatusCode();

            if (! preg_match('/^20[0-9]$/', $code))
                throw new ProtocolException(__METHOD__ . " HTTP call to ERS failed, code $code", $code);
            
            return true;
        }
        catch (\GuzzleHttp\Exception\RequestException $e)
        {
            $response = $e->getResponse();
            $code = 400;  // Default to a generic client side error code

            if (! Helpers::isnull($response))
            {
                $code = $response->getStatusCode();
            }

            if ($code == 404)
            {
                // Be nice and don't throw an exception when a resource isn't found.

                if (! Helpers::isnull($this->logger))
                    $this->logger->notice(__METHOD__ . " 404 Resource Not Found");

                return NULL;
            }
            else
            {
                if (! Helpers::isnull($this->logger))
                    $this->logger->error(__METHOD__ . " Request failed with code $code");

                throw new ProtocolException(__METHOD__ . " Guzzle RequestException: " . $e->getMessage(), $code, $e);
            }

        }
        catch (\InvalidArgumentException $e)
        {
            if (! Helpers::isnull($this->logger))
                $this->logger->error(__METHOD__ . " internal code error: " . $e->getMessage());

            return false;
        }
    }


    /**
     * getResourceCount
     *
     * Get the number of existing resources of the specified type.
     *
     * @param string $resource_type Type of resource to query, lower case
     * @param string $filter Optional search filter like "groupId.EQ.0f5e5ca0-fffa-11e9-bb8b-6a087bf18de7"
     * @return int The number of resources found that match the specified criteria
     */
    public function getResourceCount($resource_type, $filter = NULL)
    {
        if (Helpers::isnull($resource_type))
            throw new \InvalidArgumentException(__METHOD__ . " Expected parameter resource_type");

        $search_results = new SearchResults(["collection_type" => "resources",
                                             "pagelimit" => $this->getConParams()->getPageLimit(),
                                             "recordlimit" => $this->getConParams()->getRecordLimit(),
        ]);

        $auth = [$this->getConParams()->getUName(), $this->getConParams()->getUPass()];

        try
        {
            // Get only the first record from the first page, just enough to get us
            // a successful search with a record count
            $queryParams['size'] = 1;
            $queryParams['page'] = 1;

            if (! Helpers::isnull($filter))
            {
                $queryParams["filter"] = $filter;
            }

            $this->incAndCheckTxCount();

            $response = $this->getClient()->get("/ers/config/$resource_type/", [
                'headers' => $this->getStdHeaders(),
                'query' => $queryParams,
                'auth' => $auth,
                'delay' => $this->getConParams()->getTxDelay(),
            ]);

            $code = $response->getStatusCode();

            if (! preg_match('/^20[0-9]$/', $code))
                throw new ProtocolException(__METHOD__ . " HTTP call to ERS failed, code $code", $code);

            $body = (string) $response->getBody();

            $search_results->pushResults($body);

            return $search_results->getTotalRecords();

        }
        catch (\GuzzleHttp\Exception\RequestException $e)
        {
            $response = $e->getResponse();
            $code = 400;  // Default to a generic client side error code

            if (! Helpers::isnull($response))
            {
                $code = $response->getStatusCode();
            }

            if ($code == 404)
            {
                // Be nice and don't throw an exception when a resource isn't found.

                if (! Helpers::isnull($this->logger))
                    $this->logger->notice(__METHOD__ . " 404 Resource Not Found");

                return 0;
            }
            else
            {
                if (! Helpers::isnull($this->logger))
                    $this->logger->error(__METHOD__ . " Request failed with code $code");

                throw new ProtocolException(__METHOD__ . " Guzzle RequestException: " . $e->getMessage(), $code, $e);
            }
        }

        return 0;
    }


    private function doDestruct()
    {
        if (! Helpers::isnull($this->logger))
            $this->logger->debug(__METHOD__ . " cleaning up after " . $this->getTxCount() . " transactions");

        // Release the client
        $this->setClient(NULL);
        $this->resetTxCount();
    }

    public function __destruct()
    {
        $this->doDestruct();
    }
}

?>
