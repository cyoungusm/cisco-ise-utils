<?php

namespace ISEUtils\ERS;

use ISEUtils\Helpers;
use ISEUtils\Uuid;

/**
 * ResourceBase class
 *
 * All resource classes should inherit from ResourceBase
 *
 * @see https://developer.cisco.com/docs/identity-services-engine/#!creating-a-resource
 *
 */
class ResourceBase
{

    public const RESOURCE_TYPE = "Unknown";

    /**
     * @var string $_id - Maps to id String property on all resources
     * @var string $_name - Maps to name String property on all resources
     * @var string $_description - Maps to description String property on all resources
     */
    protected $_id = "";
    protected $_name = "";
    protected $_description = "";

    /**
     * @var array $_link - Not an official resource property, but this
     * field is sometimes returned in search results.
     */
    protected $_link = [];

    /**
     * @var bool $_partial - Not an official resource property.  This flag
     * indicates that we only have partial data for this resource.  This often
     * is the case just after creating a new resource.  ERS generates things
     * like unique IDs that aren't returned in the creation response. Keeping
     * track of this with the _partial flag helps us recognize that situation.
     */
    protected $_partial = true;

    /**
     * getResourceType
     *
     * @return string - The resource type of the object.  Expected to be
     *                  overridden by child classes.
     */
    public function getResourceType()
    {
        return ResourceBase::RESOURCE_TYPE;
    }

    /**
     * getJsonObjectName()
     *
     * @return string - The object name used in JSON serialization of the
     *                  object.  Expected to be overridden by child classes.
     *                  Normally, the ResourceType is the same as the JSON
     *                  object name, but for some resources, they differ.  Ask
     *                  Cisco why, not me.
     *                  
     */
    public function getJsonObjectName()
    {
        return ResourceBase::RESOURCE_TYPE;
    }

    public function getID()      { return $this->_id;          }
    public function getName()    { return $this->_name;        }
    public function getDesc()    { return $this->_description; }
    public function getLink()    { return $this->_link;        }
    public function getPartial() { return $this->_partial;     }

    public function setID($val)      { $this->_id = $val;          }
    public function setName($val)    { $this->_name = $val;        }
    public function setDesc($val)    { $this->_description = $val; }
    public function setLink($val)    { $this->_link = $val;        }
    public function setPartial($val) { $this->_partial = $val;        }

    /**
     * toArray
     *
     * @return array - An array representation of the object.
     *
     * Child classes who override need to call this parent method first, then
     * add in their own fields.
     */
    public function toArray()
    {
        if (! Helpers::isnull($this->getLink()))
            return [
                $this->getJsonObjectName() => [
                    "id" => $this->getID(),
                    "name" => $this->getName(),
                    "description" => $this->getDesc(),
                    'link' => $this->getLink()
                ]
            ];
        else
            return [
                $this->getJsonObjectName() => [
                    "id" => $this->getID(),
                    "name" => $this->getName(),
                    "description" => $this->getDesc()
                ]
            ];            
    }

    
    /**
     * toJSON
     *
     * @return string - A JSON serialized representation of the object.
     *
     */
    public function toJSON()
    {
        return json_encode($this->toArray());
    }

    /**
     * isResource (static)
     *
     * Checks if $obj is a ResourceBase or derives from ResourceBase
     *
     * @param ResourceBase|ChildClass $obj - Object to check
     * @return bool - true if $obj is or derives from a ResourceBase
     */
    public static function isResource($obj)
    {
        if ( is_subclass_of($obj, 'ISEUtils\ResourceBase', true) || ($obj instanceof \ISEUtils\ResourceBase) )
            return true;
        else
            return false;
    }

    /**
     * isCompatibleType
     *
     * @param mixed $obj
     * @return bool - true if $obj has a compatible interface with ResourceBase
     */
    public function isCompatibleType($obj)
    {
        return ResourceBase::isResource($obj);
    }

    /**
     * __construct
     *
     * Construct a ResourceBase.  The function accepts $params as either an
     * array representation of a ResourceBase (or derived class) or an actual
     * instance of a ResourceBase (or derived class).
     *
     * For creating new objects, normally you pass in the array version.  The
     * other version is a copy constructor (for convenience).
     *
     * @param array|ResourceBase|ChildClass $params
     */
    public function __construct($params)
    {
        if (Helpers::isnull($params))
            throw new \InvalidArgumentException(__METHOD__ . " \$params is NULL");

        $this->init($params);
    }

    /**
     * init
     *
     * Does the bulk of the constructor.
     *
     * @param array|ResourceBase|ChildClass $params
     */
    private function init($initData)
    {
        if (! (is_array($initData) || $this->isCompatibleType($initData) ))
            throw new \InvalidArgumentException(__METHOD__ . " \$params not correct type: ". get_class($initData));

        if (is_array($initData))
        {
            if (array_key_exists($this->getJsonObjectName(), $initData))
                $initData = $initData[$this->getJsonObjectName()];

            if (array_key_exists("id", $initData))
                $this->setID($initData["id"]);

            if (array_key_exists("name", $initData))
                $this->setName($initData["name"]);

            if (array_key_exists("description", $initData))
                $this->setDesc($initData["description"]);

            if (array_key_exists("link", $initData))
                $this->setLink($initData["link"]);

        }
        else
        {
            $this->setID($initData->getID());
            $this->setName($initData->getName());
            $this->setDesc($initData->getDesc());
            $this->setLink($initData->getLink());
        }

        // There is no point in generating a new ID.  ISE creates it's own IDs and ignores
        // any you pass in when creating a resource.
        //
        // if (Helpers::isnull( $this->_id ) )
        //     $this->setID(Uuid::uuid4());

    }
    
}

?>
