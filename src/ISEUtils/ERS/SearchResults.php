<?php

namespace ISEUtils\ERS;

use ISEUtils\Helpers;

class SearchResults
{
    protected $_collectionType = "";
    protected $_totalRecords = 0;
    protected $_results = [];
    protected $_hasMoreResults = false;
    protected $_nextPageHref = "";
    protected $_recordsRead = 0;
    protected $_pagesRead = 0;
    protected $_pageLimit = 0;
    protected $_recordLimit = 0;

    public function getCollectionType() { return $this->_collectionType; }
    public function getTotalRecords()   { return $this->_totalRecords; }
    public function getHasMoreResults() { return $this->_hasMoreResults; }
    public function getNextPageHref()   { return $this->_nextPageHref; }
    public function getRecordsRead()    { return $this->_recordsRead; }
    public function getPagesRead()      { return $this->_pagesRead; }
    public function getPageLimit()      { return $this->_pageLimit; }
    public function getRecordLimit()    { return $this->_recordLimit; }

    protected function setCollectionType($val) { $this->_collectionType = $val; }
    protected function setTotalRecords($val)   { $this->_totalRecords = $val; }
    protected function setResults($val)        { $this->_results = $val; }
    protected function setHasMoreResults($val) { $this->_hasMoreResults = $val; }
    protected function setNextPageHref($val)   { $this->_nextPageHref = $val; }
    protected function setRecordsRead($val)    { $this->_recordsRead = $val; }
    protected function setPagesRead($val)      { $this->_pagesRead = $val; }
    public function setPageLimit($val)         { $this->_pageLimit = $val; }
    public function setRecordLimit($val)       { $this->_recordLimit = $val; }

    protected function appendResults($collection) { $this->setResults(array_merge($this->getResults(), $collection)); }

    public function getResults()
    {
        if ($this->getRecordLimit() > 0)
            return array_slice($this->_results, 0, $this->getRecordLimit(), true);
        else
            return $this->_results;
    }

    public function __construct($params)
    {
        if (! is_array($params) )
            throw new \InvalidArgumentException("Expected array \$params");

        if (! array_key_exists('collection_type', $params))
            throw new \InvalidArgumentException("Expected parameter collection_type");

        // For the moment, the only type we really use is "resources"
        $this->setCollectionType($params["collection_type"]);

        // Optional parameters
        if (array_key_exists('pagelimit', $params))
            $this->setPageLimit($params['pagelimit']);

        if (array_key_exists('recordlimit', $params))
            $this->setRecordLimit($params['recordlimit']);

        if (Helpers::isnull($this->getCollectionType()))
            throw new \InvalidArgumentException("Expected non-null collection_type param");
    }

    public function hitPageLimit()
    {
        // Do we have a configured page limit?
        if ($this->getPageLimit() > 0)
        {
            // Have we passed it?
            if ($this->getPagesRead() >= $this->getPageLimit())
                return true;
        }

        return false;
    }

    public function hitRecordLimit()
    {
        // Do we have a configured record limit?
        if ($this->getRecordLimit() > 0)
        {
            if ($this->getRecordsRead() >= $this->getRecordLimit())
                return true;
        }

        return false;
    }


    public function pushResults($data)
    {
        if (Helpers::isnull($data))
            throw new \InvalidArgumentException("SearchResults::pushResults() Expected parameter \$data");

        if (Helpers::isnull($this->getResults()))
        {
            // echo "First chunk!".PHP_EOL;
        
            // We got our first chunk of data
            $decodedCurrent = json_decode($data, true);

            if (array_key_exists('SearchResult', $decodedCurrent))
            {
                $collection = [];
                $nextPage = [];

                if (array_key_exists('total', $decodedCurrent["SearchResult"]))
                    $this->setTotalRecords(intval($decodedCurrent["SearchResult"]["total"]));

                if (array_key_exists($this->_collectionType, $decodedCurrent["SearchResult"]))
                    $collection = $decodedCurrent["SearchResult"][$this->_collectionType];

                if (array_key_exists('nextPage', $decodedCurrent["SearchResult"]))
                    $nextPage = $decodedCurrent["SearchResult"]["nextPage"];

                if (! Helpers::isnull($collection))
                {
                    $this->setPagesRead( $this->getPagesRead() + 1 );
                    $this->setRecordsRead( $this->getRecordsRead() + count($collection) );
                    $this->setResults( $collection );

                    if (! Helpers::isnull($nextPage))
                    {
                        if  (array_key_exists('href', $nextPage))
                        {
                            $this->setHasMoreResults(true);
                            $this->setNextPageHref($nextPage['href']);
                        }
                        else
                            throw new \ProtocolException("SearchResults::pushResults() got a nextPage without an href");
                    }
                }
            }
            else
            {
                throw new \InvalidArgumentException("SearchResults::pushResults() Expected SearchResults");
            }
        } else {
            // echo "Next chunk!".PHP_EOL;
            $collection = [];
            $nextPage = [];

            // We got a subsequent chunk of data
            $decodedCurrent = NULL;
            $decodedCurrent = json_decode($data, true);

            if (array_key_exists('SearchResult', $decodedCurrent))
            {
                if (array_key_exists('total', $decodedCurrent["SearchResult"]))
                {
                    // If our total changed, print a warning
                    if ($this->getTotalRecords() != intval($decodedCurrent["SearchResult"]["total"]))
                        echo "WARNING: SearchResults->pushResults(): Server total value changed to ".intval($decodedCurrent["SearchResult"]["total"]).PHP_EOL;
                }

                if (array_key_exists($this->getCollectionType(), $decodedCurrent["SearchResult"]))
                    $collection = $decodedCurrent["SearchResult"][$this->getCollectionType()];

                if (array_key_exists('nextPage', $decodedCurrent["SearchResult"]))
                    $nextPage = $decodedCurrent["SearchResult"]["nextPage"];

                if (! Helpers::isnull($collection))
                {
                    // var_dump($this->totalResults);
                    // echo PHP_EOL.PHP_EOL;
                    // var_dump($collection);
                    // echo PHP_EOL.PHP_EOL;

                    $this->setPagesRead( $this->getPagesRead() + 1 );
                    $this->setRecordsRead( $this->getRecordsRead() + count($collection) );
                    $this->appendResults($collection);

                    if (! Helpers::isnull($nextPage))
                    {
                        if  (array_key_exists('href', $nextPage))
                        {
                            $this->setHasMoreResults(true);
                            $this->setNextPageHref($nextPage['href']);
                        }
                        else
                            throw new \ProtocolException("SearchResults::pushResults() got a nextPage without an href");
                    }
                    else
                    {
                        $this->setHasMoreResults(false);
                        $this->setNextPageHref("");
                    }

                }
            }
            else
            {
                throw new \InvalidArgumentException("SearchResults::pushResults() Expected SearchResults");
            }

        }


    }

    

}


?>
