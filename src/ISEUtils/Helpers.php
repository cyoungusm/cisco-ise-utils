<?php

namespace ISEUtils;

class Helpers
{

    public static function isnull($value)
    {
        if (is_string($value))
        {
            return ! (strlen(trim($value)) > 0);
        }
        elseif (is_object($value))
        {
            return $value === NULL;
        }
        elseif (is_int($value))
        {
            return $value == 0;
        }
        elseif (is_float($value))
        {
            // questionable... maybe use a delta
            return $value == 0.0;
        }
        elseif (is_array($value))
        {
            // TODO: Check for an array of all empty arrays
            return ! ( sizeof($value) > 0 );
        }
        else
        {
            return is_null($value);
        }
    }

}

?>
