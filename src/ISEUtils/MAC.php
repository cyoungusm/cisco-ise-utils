<?php

namespace ISEUtils;

class MAC
{
    public static function randomMAC()
    {
        $data = openssl_random_pseudo_bytes(6);
        assert(strlen($data) == 6);

        $data[0] = chr(0x02);

        return vsprintf('%s:%s:%s:%s:%s:%s', str_split(bin2hex($data), 2));
    }

}

?>
