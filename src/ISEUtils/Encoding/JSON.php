<?php

namespace ISEUtils\Encodings;

use ISEUtils\Helpers;
use ISEUtils\Exceptions\ProtocolException;

// TODO: Eventually move to setting JSON_THROW_ON_ERROR to let json_ functions throw their own exceptions
class JSON
{
    public static function decode($jsonData)
    {
        if ( Helpers::isnull($jsonData) )
            return NULL;

        $decodedData = json_decode((string) $jsonData, true);

        if (json_last_error() !== JSON_ERROR_NONE)
            throw new ProtocolException(__METHOD__ . " Unable to decode <" . (string)$jsonData . "> " . json_last_error_msg() );

        return $decodedData;
    }

    public static function encode($obj)
    {
        if ( Helpers::isnull($obj) )
            return NULL;

        $encodedData = json_encode($obj, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);

        if (json_last_error() !== JSON_ERROR_NONE)
            throw new ProtocolException(__METHOD__ . " Unable to encode obj " . json_last_error_msg() );

        return $encodedData;
    }

}

?>
