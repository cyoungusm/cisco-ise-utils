<?php
namespace ISEUtils\ISE;

use ISEUtils\Helpers;
use ISEUtils\Exceptions\ProtocolException;
use ISEUtils\ERS\SearchResults;
use ISEUtils\ERS\ERSClient;
use ISEUtils\ERS\ConnectionParams;
use ISEUtils\ISE\ResourceFactory;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * ISEUtils\ISE\Client class
 *
 * Provides a client for Cisco Identity Services Engine accessed over
 * Cisco External RESTful services.
 *
 * @see ISEUtils\ERS\ERSClient - The client used here for RESTful access
 *
 */
class Client
{
    /**
     * @var ISEUtils\ERS\ERSClient  $_ersclient - handle for our RESTful client
     */
    private $_ersclient = NULL;

    private function getClient()     { return $this->_ersclient; }
    private function setClient($val) { $this->_ersclient = $val; }

    /**
     * This class is PSR-3 "Logger Aware"
     */
    use LoggerAwareTrait;

    /**
     * __construct
     *
     * @param ConnectionParams $coninfo A ConnectionParams object with connection details
     * @param array $params An array with the following optional elements:
     * <ul>
     *  <li>\Psr\Log\LoggerInterface logger - A PSR-3 compliant logger object for logging library level events</li>
     *  <li>\Psr\Log\LoggerInterface messagelogger - A PSR-3 compliant logger object for logging underlying communications (Guzzle)</li>
     * </ul>
     *
     */
    public function __construct($coninfo, $params = NULL)
    {
        if (Helpers::isnull($coninfo) || ! ($coninfo instanceof ConnectionParams) )
            throw new \InvalidArgumentException(__METHOD__ . " Expected param $coninfo type ConnectionParams");

        if (! Helpers::isnull($params) && is_array($params) )
        {
            if (array_key_exists('logger', $params))
            {
                if (! Helpers::isnull($params['logger']) && $params['logger'] instanceof LoggerInterface)
                    $this->setLogger($params['logger']);
            }
        }
        
        // Create our ERS client.
        $this->setClient(new ERSClient($coninfo, $params));

        if (! Helpers::isnull($this->logger))
            $this->logger->debug(__METHOD__ . " Created an internal rest client handle.");

    }

    /**
     * getEndPoints
     *
     * Gets all EndPoint resources.  Warning: this returns a lot of
     * records.  Consider setting page/record limits.
     *
     * @return array An array of EndPoint objects returned by the search or NULL if no EndPoints are found
     */
    public function getEndPoints()
    {
        $resource_type = strtolower(EndPoint::RESOURCE_TYPE);

        $results = $this->getClient()->getResources($resource_type);

        if ( ! Helpers::isnull($results) )
        {

            // Build an array of the appropriate resource objects to be returned 
            $resource_collection = [];
            foreach ($results as $index => $resource_params)
            {
                $resource_collection[] = ResourceFactory::constructResource($resource_type, $resource_params);
            }

            return $resource_collection;

        }
            
        if (! Helpers::isnull($this->logger))
            $this->logger->notice(__METHOD__ . " No EndPoints were returned");

        return NULL;

    }

    /**
     * getEndPointsByGroup
     *
     * Gets all EndPoint resources that belong to a specific group.
     *
     * @param EndPointGroup $params The group under which to search for EndPoints
     * @return array An array of EndPoint objects returned by the search or NULL if no EndPoints were found
     */
    public function getEndPointsByGroup($group)
    {

        if (Helpers::isnull($group) || ! EndPointGroup::isEndPointGroup($group))
            throw new \InvalidArgumentException(__METHOD__ . " Expected EndPointGroup \$group");
            
        if (Helpers::isnull($group->getID()))
            throw new \InvalidArgumentException(__METHOD__ . " Expected ID in object \$group");

        $resource_type = strtolower(EndPoint::RESOURCE_TYPE);

        $results = $this->getClient()->getResources($resource_type, "groupId.EQ." . $group->getID());

        if ( ! Helpers::isnull($results) )
        {

            // Build an array of the appropriate resource objects to be returned 
            $resource_collection = [];
            foreach ($results as $index => $resource_params)
            {
                $resource_collection[] = ResourceFactory::constructResource($resource_type, $resource_params);
            }

            return $resource_collection;

        }
            
        if (! Helpers::isnull($this->logger))
            $this->logger->notice(__METHOD__ . " No EndPoints were returned");

        return NULL;

    }


    /**
     * getEndPointGroups
     *
     * Gets all EndPointGroup resources.  Warning: this may return a
     * lot of records.  Consider setting page/record limits.
     *
     * @return array An array of EndPointGroup objects returned by the search or NULL if no EndPointGroups were found
     */
    public function getEndPointGroups()
    {
        $resource_type = strtolower(EndPointGroup::RESOURCE_TYPE);

        $results = $this->getClient()->getResources($resource_type);

        if ( ! Helpers::isnull($results) )
        {

            // Build an array of the appropriate resource objects to be returned 
            $resource_collection = [];
            foreach ($results as $index => $resource_params)
            {
                $resource_collection[] = ResourceFactory::constructResource($resource_type, $resource_params);
            }

            return $resource_collection;

        }
            
        if (! Helpers::isnull($this->logger))
            $this->logger->notice(__METHOD__ . " No EndPointGroups were returned");

        return NULL;
            
    }


    /**
     * getEndPoint
     *
     * Get a single EndPoint
     *
     * @param string $query Resource specifier.
     *
     * This can be just an ID like '8499ba32-aa7a-11e2-8725-623ffb053f44' or a search
     * by name like 'name/00:00:48:03:0C:3F', etc.
     *
     * @return EndPoint object returned by the search or NULL if no EndPoint was found
     */
    public function getEndPoint($query)
    {
        $res = NULL;
        $resource_type = strtolower(EndPoint::RESOURCE_TYPE);

        if (Helpers::isnull($query) )
            throw new \InvalidArgumentException(__METHOD__ . " Expected string \$query");

        $results = $this->getClient()->getResource($resource_type, $query);

        if (! Helpers::isnull($results))
        {
            $res = ResourceFactory::constructResource($resource_type, $results);

            if (Helpers::isnull($res))
                throw new ProtocolException(__METHOD__ . " Unable to decode resource");

            $res->setPartial(false);
        }

        if (! Helpers::isnull($this->logger) && Helpers::isnull($res))
            $this->logger->notice(__METHOD__ . " No EndPoint was returned");

        return $res;
    }

    /**
     * getEndPointGroup
     *
     * Get a single EndPointGroup
     *
     * @param string $query Resourse specifier.
     *
     * This can be just an ID like '0f5e5ca0-fffa-11e9-bb8b-6a087bf18de7' or a search
     * by name like 'name/Student_Devices', etc.
     *
     * @return EndPointGroup object returned by the search or NULL if no EndPointGroup was found
     */
    public function getEndPointGroup($query)
    {
        $resource_type = strtolower(EndPointGroup::RESOURCE_TYPE);

        if (Helpers::isnull($query) )
            throw new \InvalidArgumentException(__METHOD__ . " Expected string \$query");

        $res = NULL;

        $results = $this->getClient()->getResource($resource_type, $query);

        if (! Helpers::isnull($results))
        {
            $res = ResourceFactory::constructResource($resource_type, $results);

            if (Helpers::isnull($res))
                throw new ProtocolException(__METHOD__ . " Unable to decode resource");

            $res->setPartial(false);
        }
        
        if (! Helpers::isnull($this->logger) && Helpers::isnull($res))
            $this->logger->notice(__METHOD__ . " No EndPointGroup was returned");

        return $res;

    }


    /**
     * createEndPoint
     *
     * Create a single, new EndPoint.
     *
     * @param EndPoint $obj EndPoint object holding data for creating a new EndPoint.
     */
    public function createEndPoint($obj)
    {
        if (! EndPoint::isEndPoint($obj))
            throw new \InvalidArgumentException(__METHOD__ . " Expected an EndPoint \$obj");
            
        $resource_type = strtolower($obj->getResourceType());

        try
        {
            return $this->getClient()->postNewResource($resource_type, $obj->toArray());
        }
        catch (\InvalidArgumentException $e)
        {
            if (! Helpers::isnull($this->logger))
                $this->logger->error(__METHOD__ . " postNewResource() failed: " . $e->getMessage());

            return NULL;
        }
    }

    /**
     * createEndPointGroup
     *
     * Create a single, new EndPointGroup.
     *
     * @param EndPointGroup $obj EndPointGroup object holding data for creating a new EndPointGroup
     */
    public function createEndPointGroup($obj)
    {
        if (! EndPointGroup::isEndPointGroup($obj))
            throw new \InvalidArgumentException(__METHOD__ . " Expected an EndPointGroup \$obj");

        $resource_type = strtolower($obj->getResourceType());

        try
        {
            return $this->getClient()->postNewResource($resource_type, $obj->toArray());
        }
        catch (\InvalidArgumentException $e)
        {
            if (! Helpers::isnull($this->logger))
                $this->logger->error(__METHOD__ . " postNewResource() failed: " . $e->getMessage());

            return NULL;
        }
    }


    /**
     * deleteEndPoint
     *
     * @param EndPoint $obj EndPoint object to delete.
     * @return bool true if $obj was deleted, false otherwise
     */
    public function deleteEndPoint($obj)
    {

        if (! EndPoint::isEndPoint($obj))
            throw new \InvalidArgumentException(__METHOD__ . " Expected an EndPoint \$obj");
            
        $resource_type = strtolower($obj->getResourceType());

        // Can happen with partially loaded objects
        if (Helpers::isnull($obj->getID()))
            throw new \InvalidArgumentException(__METHOD__ . " Expected an EndPoint with an ID value for \$obj");

        $resourceID = $obj->getID();
        $resourceSpecifier = $resourceID;

        return $this->getClient()->deleteResource($resource_type, $resourceSpecifier);

    }

    /**
     * deleteEndPointGroup
     *
     * @param EndPointGroup $obj EndPointGroup object to delete.
     * @return bool true if $obj was deleted, false otherwise
     */
    public function deleteEndPointGroup($obj)
    {

        if (! EndPointGroup::isEndPointGroup($obj))
            throw new \InvalidArgumentException(__METHOD__ . " Expected an EndPointGroup \$obj");
            
        $resource_type = strtolower($obj->getResourceType());

        // This can happen with partially loaded objects
        if (Helpers::isnull($obj->getID()))
            throw new \InvalidArgumentException(__METHOD__ . " Expected an EndPointGroup with an ID value for \$obj");

        $resourceID = $obj->getID();
        $resourceSpecifier = $resourceID;

        return $this->getClient()->deleteResource($resource_type, $resourceSpecifier);

    }

    /**
     * deleteEndPointGroup
     *
     * @param EndPointGroup $obj EndPointGroup object whose EndPoints should be deleted.
     * @return bool false if there were errors deleting EndPoints belonging to $obj, true otherwise
     */
    public function deleteEndPointsByGroup($obj)
    {

        if (! EndPointGroup::isEndPointGroup($obj))
            throw new \InvalidArgumentException(__METHOD__ . " Expected an EndPointGroup \$obj");

        if (Helpers::isnull($obj->getID()))
            throw new \InvalidArgumentException(__METHOD__ . " Expected an EndPointGroup with an ID value for \$obj");

        $errorsFound = 0;

        $resource_type = strtolower($obj->getResourceType());
        $groupID = $obj->getID();
        $resourceSpecifier = $groupID;

        $endpoints =  $this->getEndPointsByGroup($obj);
        $endpointsToDelete = array();

        if (! Helpers::isnull($endpoints) )
        {
            if (! Helpers::isnull($this->logger))
                $this->logger->debug(__METHOD__ . " Got " . count($endpoints) . " EndPoints");

            foreach ($endpoints as $endpoint)
            {
                if ($endpoint->getPartial())
                {
                    $endpointsToDelete[] = $this->getEndPoint($endpoint->getId());
                }

                if (! Helpers::isnull($this->logger))
                    $this->logger->debug(__METHOD__ . " EP:" . $endpoint->getId() . " : " . $endpoint->getName() . " : " . $endpoint->getgroupId());
            }

            foreach ($endpointsToDelete as $endpoint)
            {
                $results = $this->deleteEndPoint($endpoint);

                if ($results)
                {
                    if (! Helpers::isnull($this->logger))
                        $this->logger->info(__METHOD__ . " Deleted " . $endpoint->getName());

                } else {
                    if (! Helpers::isnull($this->logger))
                        $this->logger->alert(__METHOD__ . "Failed to delete EndPoint ". $endpoint->getId());
                    ++$errorsFound;
                }

            }

        }
        else
        {
            if (! Helpers::isnull($this->logger))
                $this->logger->notice(__METHOD__ . " No EndPoints were found belonging to group " . $obj->getID());
        }

        if ($errorsFound == 0)
            return true;

        // TODO: Maybe think about passing more detailed error info back?
        return false;
    }

    /**
     * getEndPointCount
     *
     * Get a count of the EndPoints matching the specified criteria.
     *
     * @param string $filter Optional search filter like "groupId.EQ.0f5e5ca0-fffa-11e9-bb8b-6a087bf18de7"
     * @return int The number of EndPoints matching the specified criteria
     */
    public function getEndPointCount($filter = NULL)
    {
        $resource_type = strtolower(EndPoint::RESOURCE_TYPE);

        $count = $this->getClient()->getResourceCount($resource_type, $filter);

        return $count;
    }


    public function __destruct()
    {
        if (! Helpers::isnull($this->logger))
            $this->logger->debug(__METHOD__ . " Shutting down");

        // Release the client
        $this->setClient(NULL);

    }
}

?>
