<?php

namespace ISEUtils\ISE;

use ISEUtils\Helpers;
use ISEUtils\Uuid;
use ISEUtils\ERS\ResourceBase;

/**
 * EndPoint class.
 *
 * A resource class represending a Cisco ISE Endpoint.
 *
 * @see https://developer.cisco.com/docs/identity-services-engine/#!end-point
 *
 */
class EndPoint extends ResourceBase
{
    public const RESOURCE_TYPE = "EndPoint";

    /**
     * @var array  $_customAttributes - Maps to customAttributes Map property on EndPoint
     * @var string $_groupId - Maps to groupId String property on EndPoint
     * @var string $_identityStore - Maps to identityStore String property on EndPoint
     * @var string $_identityStoreId - Maps to identityStoreId String property on EndPoint
     * @var string $_mac - Maps to mac String property on EndPoint
     * @var string $_portalUser - Maps to portalUser String property on EndPoint
     * @var string $_profileId - Maps to profileId String property on EndPoint
     * @var bool   $_staticGroupAssignment - Maps to staticGroupAssignment String property on EndPoint
     * @var bool   $_staticProfileAssignment - Maps to staticProfileAssignment String property on EndPoint
     *
     * @see https://developer.cisco.com/docs/identity-services-engine/#!end-point/resource-definition
     */
    protected $_customAttributes        = [];
    protected $_groupId                 = "";
    protected $_identityStore           = "";
    protected $_identityStoreId         = "";
    protected $_mac                     = "";
    protected $_portalUser              = "";
    protected $_profileId               = "";
    protected $_staticGroupAssignment   = false;
    protected $_staticProfileAssignment = false;
    
    /**
     * getResourceType
     *
     * @return string - The resource type of the object.
     */
    public function getResourceType()
    {
        return EndPoint::RESOURCE_TYPE;
    }

    /**
     * getJsonObjectName()
     *
     * @return string - The object name used in JSON serialization of
     *                  the object. For EndPoint resources, this
     *                  differs slightly from the resource type name.
     *                  It's ERSEndPoint instead of just EndPoint.
     */
    public function getJsonObjectName()
    {
        // For some reason, ISE sometimes uses ERSEndPoint or just EndPoint,
        // so we have to differentiate.
        return "ERS".EndPoint::RESOURCE_TYPE;
    }

    public function getcustomAttributes       () { return $this->_customAttributes;        }
    public function getgroupId                () { return $this->_groupId;                 }
    public function getidentityStore          () { return $this->_identityStore;           }
    public function getidentityStoreId        () { return $this->_identityStoreId;         }
    public function getmac                    () { return $this->_mac;                     }
    public function getportalUser             () { return $this->_portalUser;              }
    public function getprofileId              () { return $this->_profileId;               }
    public function getstaticGroupAssignment  () { return $this->_staticGroupAssignment;   }
    public function getstaticProfileAssignment() { return $this->_staticProfileAssignment; }

    public function setcustomAttributes       ($val) { $this->_customAttributes = $val;        }
    public function setgroupId                ($val) { $this->_groupId = $val;                 }
    public function setidentityStore          ($val) { $this->_identityStore = $val;           }
    public function setidentityStoreId        ($val) { $this->_identityStoreId = $val;         }
    public function setmac                    ($val) { $this->_mac = $val;                     }
    public function setportalUser             ($val) { $this->_portalUser = $val;              }
    public function setprofileId              ($val) { $this->_profileId = $val;               }
    public function setstaticGroupAssignment  ($val) { $this->_staticGroupAssignment = $val;   }
    public function setstaticProfileAssignment($val) { $this->_staticProfileAssignment = $val; }

    /**
     * isEndPoint (static)
     *
     * Checks if $obj is an EndPoint or derives from it.
     *
     * @param EndPoint|ChildClass $obj - Object to check
     * @return bool - true if $obj is or derives from a EndPoint
     */
    public static function isEndPoint($obj)
    {
        if ( is_subclass_of($obj, 'ISEUtils\ISE\EndPoint', true) || ($obj instanceof \ISEUtils\ISE\EndPoint) )
            return true;
        else
            return false;
    }

    /**
     * isCompatibleType
     *
     * @param mixed $obj
     * @return bool - true if $obj has a compatible interface with EndPoint
     */
    public function isCompatibleType($obj)
    {
        return EndPoint::isEndPoint($obj);
    }

    /**
     * __construct
     *
     * Construct an EndPoint.  The function accepts $params as either an array
     * representation of an EndPoint (or derived class) or an actual instance of
     * an EndPoint (or derived class).
     *
     * For creating new objects, normally you pass in the array version.  The
     * other version is a copy constructor (for convenience).
     *
     * @param array|EndPoint|ChildClass $params
     */
    public function __construct($params)
    {
        if (Helpers::isnull($params))
            throw new \InvalidArgumentException(__METHOD__ . " \$params is NULL");

        parent::__construct($params);

        $this->init($params);
    }

    /**
     * init
     *
     * Does the bulk of the constructor.
     *
     * @param array|EndPoint|ChildClass $params
     */
    private function init($initData)
    {
        if ( ! (is_array($initData) || $this->isCompatibleType($initData) ) )
            throw new \InvalidArgumentException(__METHOD__ . " \$initData not correct type: ". get_class($initData));

        if (is_array($initData))
        {
            if (array_key_exists($this->getJsonObjectName(), $initData))
                $initData = $initData[$this->getJsonObjectName()];

            if (array_key_exists("customAttributes", $initData))
                $this->setcustomAttributes($initData["customAttributes"]);

            if (array_key_exists("groupId", $initData))
                $this->setgroupId($initData["groupId"]);

            if (array_key_exists("identityStore", $initData))
                $this->setidentityStore($initData["identityStore"]);

            if (array_key_exists("identityStoreId", $initData))
                $this->setidentityStoreId($initData["identityStoreId"]);

            if (array_key_exists("mac", $initData))
                $this->setmac($initData["mac"]);

            if (array_key_exists("portalUser", $initData))
                $this->setportalUser($initData["portalUser"]);

            if (array_key_exists("profileId", $initData))
                $this->setprofileId($initData["profileId"]);

            if (array_key_exists("staticGroupAssignment", $initData))
                $this->setstaticGroupAssignment($initData["staticGroupAssignment"]);

            if (array_key_exists("staticProfileAssignment", $initData))
                $this->setstaticProfileAssignment($initData["staticProfileAssignment"]);

        } else {
            // Copy any members that are specific to EndPoint
            $this->setcustomAttributes($initData->getcustomAttributes());
            $this->setgroupId($initData->getgroupId());
            $this->setidentityStore($initData->getidentityStore());
            $this->setidentityStoreId($initData->getidentityStoreId());
            $this->setmac($initData->getmac());
            $this->setportalUser($initData->getportalUser());
            $this->setprofileId($initData->getprofileId());
            $this->setstaticGroupAssignment($initData->getstaticGroupAssignment());
            $this->setstaticProfileAssignment($initData->getstaticProfileAssignment());
        }
    }

    /**
     * toArray
     *
     * @return array - An array representation of the object.
     *
     */
    public function toArray()
    {
        $ret = parent::toArray();

        // Only include this if it has data, otherwise ERS gives us an error 400
        if (! Helpers::isnull($this->getcustomAttributes()) )
            $ret[$this->getJsonObjectName()]["customAttributes"] = $this->getcustomAttributes();
        
        $ret[$this->getJsonObjectName()]["groupId"] = $this->getgroupId();
        $ret[$this->getJsonObjectName()]["identityStore"] = $this->getidentityStore();
        $ret[$this->getJsonObjectName()]["identityStoreId"] = $this->getidentityStoreId();
        $ret[$this->getJsonObjectName()]["mac"] = $this->getmac();
        $ret[$this->getJsonObjectName()]["portalUser"] = $this->getportalUser();
        $ret[$this->getJsonObjectName()]["profileId"] = $this->getprofileId();
        $ret[$this->getJsonObjectName()]["staticGroupAssignment"] = $this->getstaticGroupAssignment();
        $ret[$this->getJsonObjectName()]["staticProfileAssignment"] = $this->getstaticProfileAssignment();

        return $ret;
    }
}

?>
