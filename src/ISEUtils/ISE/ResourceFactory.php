<?php

namespace ISEUtils\ISE;

use ISEUtils\Helpers;
use ISEUtils\ERS\ResourceBase;
use ISEUtils\ISE\EndPointGroup;
use ISEUtils\ISE\EndPoint;

class ResourceFactory
{
    public static function constructResource($type, $params)
    {
        if ( Helpers::isnull($type) || ! is_string($type))
            throw new \InvalidArgumentException("Expected non-null string \$type");

        switch (strtolower($type))
        {
        case strtolower(EndPointGroup::RESOURCE_TYPE):
            return new EndPointGroup($params);
            break;

        case strtolower(EndPoint::RESOURCE_TYPE):
            return new EndPoint($params);
            break;

        default:
            throw new \InvalidArgumentException("Unknown Resource Type $type");

        }

    }

    public static function isResource($obj)
    {
        return ResourceBase::isResource($obj);
    }

}
