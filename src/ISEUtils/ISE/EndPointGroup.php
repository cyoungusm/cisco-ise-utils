<?php

namespace ISEUtils\ISE;

use ISEUtils\Helpers;
use ISEUtils\Uuid;
use ISEUtils\ERS\ResourceBase;

/**
 * EndPointGroup class.
 *
 * A resource class represending a Cisco ISE Endpoint Identity Group.
 *
 * @see https://developer.cisco.com/docs/identity-services-engine/#!endpoints-identity-group
 *
 */
class EndPointGroup extends ResourceBase
{
    public const RESOURCE_TYPE = "EndPointGroup";

    /**
     * @var bool $_systemDefined - Maps to systemDefined Boolean propery on EndPointGroup
     *
     * @see https://developer.cisco.com/docs/identity-services-engine/#!endpoints-identity-group/resource-definition
     */
    protected $_systemDefined = false;

    /**
     * getResourceType
     *
     * @return string - The resource type of the object.
     */
    public function getResourceType()
    {
        return EndPointGroup::RESOURCE_TYPE;
    }

    /**
     * getJsonObjectName()
     *
     * @return string - The object name used in JSON serialization of the
     *                  object.
     */
    public function getJsonObjectName()
    {
        return EndPointGroup::RESOURCE_TYPE;
    }


    public function getSysDef() { return $this->_systemDefined; }
    public function setSysDef($val) { $this->_systemDefined = $val; }

    /**
     * isEndPointGroup (static)
     *
     * Checks if $obj is an EndPointGroup or derives from it.
     *
     * @param EndPointGroup|ChildClass $obj - Object to check
     * @return bool - true if $obj is or derives from a EndPointGroup
     */
    public static function isEndPointGroup($obj)
    {
        if ( is_subclass_of($obj, 'ISEUtils\ISE\EndPointGroup', true) || ($obj instanceof \ISEUtils\ISE\EndPointGroup) )
            return true;
        else
            return false;
    }

    /**
     * isCompatibleType
     *
     * @param mixed $obj
     * @return bool - true if $obj has a compatible interface with EndPointGroup
     */
    public function isCompatibleType($obj)
    {
        return EndPointGroup::isEndPointGroup();
    }

    /**
     * __construct
     *
     * Construct an EndPointGroup.  The function accepts $params as either an
     * array representation of an EndPointGroup (or derived class) or an actual
     * instance of an EndPointGroup (or derived class).
     *
     * For creating new objects, normally you pass in the array version.  The
     * other version is a copy constructor (for convenience).
     *
     * @param array|EndPointGroup|ChildClass $params
     */
    public function __construct($params)
    {
        if (Helpers::isnull($params))
            throw new \InvalidArgumentException(__METHOD__ . " \$params is NULL");

        parent::__construct($params);

        $this->init($params);

    }

    /**
     * init
     *
     * Does the bulk of the constructor.
     *
     * @param array|EndPointGroup|ChildClass $params
     */
    private function init($initData)
    {
        if ( ! (is_array($initData) || $this->isCompatibleType($initData) ) )
            throw new \InvalidArgumentException(__METHOD__ . " initData not correct type: ". get_class($initData));

        if ( is_array($initData) )
        {
            if (array_key_exists($this->getJsonObjectName(), $initData))
                $initData = $initData[$this->getJsonObjectName()];

            if (array_key_exists("systemDefined", $initData))
                $this->setSysDef($initData["systemDefined"]);
        }
        else
        {
            $this->setSysDef($initData->getSysDef());
        }

    }

    /**
     * toArray
     *
     * @return array - An array representation of the object.
     *
     */
    public function toArray()
    {
        $ret = parent::toArray();
        $ret[$this->getJsonObjectName()]["systemDefined"] = $this->getSysDef();
        return $ret;
    }
}

?>
