<?php
namespace ISEUtils\Tests;

use PHPUnit\Framework\TestCase;

use ISEUtils\Encodings\JSON;
use ISEUtils\ISE\EndPoint;
use ISEUtils\ISE\EndPointGroup;

class EncodingJSONTest extends TestCase
{
    public function testEncode_Null()
    {
        try
        {
            self::assertNull( JSON::encode( NULL ) );
        }
        catch (\Throwable | \Exception $e)
        {
            self::assertNull(__METHOD__ . " threw an exception " . $e->getMessage());
        }
            
    }

    public function testEncode_EmptyString()
    {
        try
        {
            self::assertNull( JSON::encode( (string) "" ) );
        }
        catch (\Throwable | \Exception $e)
        {
            self::assertNull(__METHOD__ . " threw an exception " . $e->getMessage());
        }
    }

    public function testEncode_Endpoint()
    {
        try
        {
            $ep = new EndPoint([
                "id" => "99a4cb11-ae88-11e9-8724-623ffb053f44",
                "name" => "00:00:00:01:2B:8E",
                "link" => [
                    "rel" => "self",
                    "href" => "https://isehost.domain.com:9060/ers/config/endpoint/name/00:00:00:01:2B:8E",
                    "type" => "application/json"
                ],
                "groupId" => "aa10ae00-8bff-11e6-996c-525400b48521",
                "identityStore" => "",
                "identityStoreId" => "",
                "mac" => "00:00:00:01:2B:8E",
                "portalUser" => "",
                "profileId" => "44031480-8c00-11e6-996c-525400b48521",
                "staticGroupAssignment" => false,
                "staticProfileAssignment" => false
            ]);

            $ep->setPartial(false);

            // Not sure why, but our example data don't include even empty description fields
            $objArray = $ep->toArray();
            unset($objArray["ERSEndPoint"]["description"]);
            $jsonText = JSON::encode( $objArray );

            self::assertNotNull($jsonText);

            self::assertJsonStringEqualsJsonFile(
                'exampledata/Resource-EndPoint.json', $jsonText
            );

        }
        catch (\Throwable | \Exception $e)
        {
            self::assertNull(__METHOD__ . " threw an exception " . $e->getMessage());
        }
    }

    public function testEncode_EndpointGroup()
    {
        try
        {
            $epg = new EndPointGroup([
                "id" => "0f5e5ca0-fffa-11e9-bb8b-6a087bf18de7",
                "name" => "Student_Devices",
                "description" => "Self Registration Devices by Students",
                "systemDefined" => false,
                "link" => [
                    "rel" => "self",
                    "href" => "https://isehost.domain.com:9060/ers/config/endpointgroup/name/Student_Devices",
                    "type" => "application/json"
                ]
            ]);

            $epg->setPartial(false);

            $objArray = $epg->toArray();
            $jsonText = JSON::encode( $objArray );

            self::assertNotNull($jsonText);

            self::assertJsonStringEqualsJsonFile(
                'exampledata/Resource-EndPointGroup.json', $jsonText
            );

        }
        catch (\Throwable | \Exception $e)
        {
            self::assertNull(__METHOD__ . " threw an exception " . $e->getMessage());
        }
    }


    public function testDecode_Null()
    {
        try
        {
            self::assertNull( JSON::decode( NULL ) );
        }
        catch (\Throwable | \Exception $e)
        {
            self::assertNull(__METHOD . " threw an exception " . $e->getMessage());
        }
            
    }

    public function testDecode_EmptyString()
    {
        try
        {
            self::assertNull( JSON::decode( (string) "" ) );
        }
        catch (\Throwable | \Exception $e)
        {
            self::assertNull(__METHOD . " threw an exception " . $e->getMessage());
        }
    }



}
