<?php
namespace ISEUtils\Tests;

use PHPUnit\Framework\TestCase;

use ISEUtils\Helpers;

class HelpersTest extends TestCase
{
    public function testIsNull_Null()
    {
        self::assertTrue( Helpers::isnull(NULL) );
    }

    public function testIsNull_EmptyString()
    {
        self::assertTrue( Helpers::isnull((string) "") );
    }

    public function testIsNull_IntZero()
    {
        self::assertTrue( Helpers::isnull( (int) 0) );
    }

    public function testIsNull_FloatZero()
    {
        self::assertTrue( Helpers::isnull( (float) 0.0) );
    }

    public function testIsNull_EmptyArray()
    {
        self::assertTrue( Helpers::isnull( array() ) );
    }

    public function testNotIsNull_String()
    {
        self::assertFalse( Helpers::isnull( (string) "this is a string") );
    }

    public function testNotIsNull_StringIntZero()
    {
        self::assertFalse( Helpers::isnull( (string) "0") );
    }

    public function testNotIsNull_StringNullLC()
    {
        self::assertFalse( Helpers::isnull( (string) "null") );
    }

    public function testNotIsNull_StringNullUC()
    {
        self::assertFalse( Helpers::isnull( (string) "NULL") );
    }

    public function testNotIsNull_StringNullMC()
    {
        self::assertFalse( Helpers::isnull( (string) "Null") );
    }

    public function testNotIsNull_StringFloatZero()
    {
        self::assertFalse( Helpers::isnull( (string) "0.0") );
    }

    public function testNotIsNull_ArrayEmptyArray()
    {
        self::assertFalse( Helpers::isnull( array(array()) ) );
    }

    public function testNotIsNull_ArrayEmptyString()
    {
        self::assertFalse( Helpers::isnull( array( "" ) ) );
    }

    public function testNotIsNull_ArrayString()
    {
        self::assertFalse( Helpers::isnull( array( "this is a string" ) ) );
    }

    public function testNotIsNull_ArrayStrings()
    {
        self::assertFalse( Helpers::isnull( array( "this is a string", "this is another string" ) ) );
    }

    public function testNotIsNull_ArrayMixed00()
    {
        self::assertFalse( Helpers::isnull( array( 0, 4 => "this is a string", new Helpers() ) ) );
    }

    public function testNotIsNull_IntOne()
    {
        self::assertFalse( Helpers::isnull( (int) 1 ) );
    }

    public function testNotIsNull_IntTwo()
    {
        self::assertFalse( Helpers::isnull( (int) 2 ) );
    }

    public function testNotIsNull_IntMax()
    {
        self::assertFalse( Helpers::isnull( (int) PHP_INT_MAX ) );
    }

    public function testNotIsNull_IntMaxPlusOne()
    {
        self::assertFalse( Helpers::isnull( (int) PHP_INT_MAX + 1 ) );
    }


}
