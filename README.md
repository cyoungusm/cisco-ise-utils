Cisco ISE Utils
===============

# cisco-ise-utils

Cisco ISE Utils is a library for working with Cisco Identity Services Engine (ISE) via their External RESTful Services (ERS) API.  Currently, it only supports JSON types (not XML) for the API.

For documentation on the ERS API, see:

https://developer.cisco.com/docs/identity-services-engine/

## Currently Supported Functionality

As of version 1.0, the only supported resource types are EndPoints and EndPointGroups.  Any contributions to add new resource types, unit tests, or refactoring would be greatly appreciated.  The code is in a very early, rough state.  Eventually, ISEUtils\ERSClient will need to be abstracted out to an interface and associated implementation, then DI injected into ISEUtils\Client.  Test cases should be created, etc, etc.

# Trying Things Out

To run through some basic tests with the library, perform the below setup and run sample scripts.  This can help you determine if the code will work for your needs.

Copy sample-scripts/settings-default.php to sample-scripts/settings.php and edit the values for your environment.  This includes the following settings.

- CISCO_ISE_HOST - The hostname or IP of your Cisco ISE installation
- CISCO_ISE_ERS_PORT - The TCP port to use when connecting (9060 by default)
- CISCO_ISE_ERS_USER - User name for the service account with appropriate ISE permissions
- CISCO_ISE_ERS_PASS - The associated password
- CISCO_ISE_ERS_PROTO - The transport protocol to use (https by default)
- CISCO_ISE_ERS_TXPERMIN - The number of ERS transacitons per minute (null/empty/0 indicates no limit)
- CISCO_ISE_ERS_TXPERCON - The number of ERS transacitons per connection (null/empty/0 indicates no limit)
- CISCO_ISE_ERS_VERIFYSSL - Whether to verify the ISE server's SSL certificate (false indicates no verification)

Run composer install to set up vendor directory with dependencies.

Run composer dump-autoload -o to set up class autoloading.

Run the scripts like:

    php ./sample-scripts/load-testing-data.php
    
    php ./sample-scripts/delete-testing-data.php

To run without having to install PHP, consider using a containerized version of PHP and composer.  A sample Dockerfile is located under buildtools.

    docker build -t buildtools ./buildtools
    
    docker run -it --rm -v $PWD:/opt -w /opt buildtools composer install
    
    docker run -it --rm -v $PWD:/opt -w /opt buildtools composer dump-autoload -o
    
    docker run -it --rm -v $PWD:/opt -w /opt buildtools php ./sample-scripts/load-testing-data.php

Note: Also baked into buildtools is PHP_UML, which comes with the phpuml utility.  You can generate UML XMI metadata from it, which can be used to create class diagrams, etc, with a tool like Umbrello.

    docker run -it --init --rm -v $PWD:/opt -w /opt buildtools phpuml -o ./meta/ ./src

# Using ISE Utils With Composer

To use the ISE Utils library in your PHP application using Composer, you can add it as a required package in your composer.json file.

    "require": {
        "cyoungusm/cisco-ise-utils": "1.0"
    }

# Running Test Cases

Run test cases like:

    ./vendor/phpunit/phpunit/phpunit ./tests

or

    docker run -it --rm -v $PWD:/opt -w /opt buildtools ./vendor/phpunit/phpunit/phpunit ./tests

